#ifndef SETOFNUMBERS_H
#define SETOFNUMBERS_H
#include <iostream>
#include <initializer_list>


using namespace std;

template <typename T>
void sort(int size, T* data)
{
    for (int i = 0; i < size; i++)
        for (int j = i + 1; j < size; j++)
            if (*(data + j) < *(data + i))
            {
                T t = *(data + i);
                *(data + i) = *(data + j);
                *(data + j) = t;
            }
}

template <typename T>
int removeDuplicates(int size, T* arr)
{
    if (size == 0 || size == 1)
        return size;

    T temp[size];
    int j = 0;
    for (int i=0; i < size - 1; i++)
        if (arr[i] != arr[i + 1])
            temp[j++] = arr[i];

    temp[j++] = arr[size - 1];
    for (int i=0; i < j; i++)
        arr[i] = temp[i];
    return j;
}


template <typename T>
class Iterator;

template <typename T>
class set
{
    private:
        int size;
        T* data;
    public:
        set();
        set(const set<T>& s);
        explicit set(std::initializer_list<std::initializer_list<T>> lst);
        explicit set(initializer_list<T> lst);

        ~set();
        int get_length();
        bool contains(const T& elem);
        void add(const T& elem);
        void remove(const T& elem);
        T* to_array();


        set<T>& merge(const set<T>& s); //union
        set<T>& intersection(const set<T>& s);
        set<T>& subtract(const set<T>& s);
        set<T>& operator =(const set<T>& lst);
        set<T>& operator +=(const set<T>& s);
        set<T>& operator *=(const set<T>& s);
        set<T>& operator /=(const set<T>& s);
        T& operator[](int index);


        template<typename _T>
        friend std::ostream& operator <<(std::ostream& os, const set<_T>& lst);
        template<typename _T>
        friend set<_T> operator +(const set<_T>& s1, const set<_T>& s2);
        template<typename _T>
        friend set<_T> operator *(const set<_T>& s1, const set<_T>& s2);
        template<typename _T>
        friend set<_T> operator /(const set<_T>& s1, const set<_T>& s2);



        template <typename _T>
        friend class Iterator;
        Iterator<T> iterator_begin();
        Iterator<T> iterator_end();
        void clear();

};





#endif // SETOFNUMBERS_H
