#include <iostream>

using namespace std;
#include "setofnumbers.h"
#include "setofnumbers.cpp"
#include "iterator.h"
#include "iterator.cpp"
int main()
{
    initializer_list<int>a = {2,5,1,5,12,8,3};
    initializer_list<int>b = {1,2,3,4,5,6,7,8,9};
    initializer_list<initializer_list<int>>arab = {a,b};
    initializer_list<int>c = {40,41,42,43,45,46};
    set<int>list({a,c});
    set<int>ar(arab);
    cout << ar << endl;
    cout << "is contain elem 100?  ";
    if (ar.contains(100))
       cout<<"YES"<<endl;
    else
       cout<<"NO"<<endl;
    cout << "is contain elem 2?   ";
    if (ar.contains(2))
       cout<<"YES"<<endl;
    else
       cout<<"NO"<<endl;

    cout << "convert set to array   " << endl;
    int* my_arr = ar.to_array();
    for (int i = 0; i < ar.get_length(); i++)
        cout<<my_arr[i]<<" ";
    cout << endl;
    set<int>arb = ar;

    set<int>s({1,-4,3,4,7});
    set<int>s2({{13,5,66,43},{1,12,-15,4}});
    cout << s << endl;
    cout << s2 << endl;
    set<int>merge;
    set<int>intersec;
    set<int>subtract;
    cout << "intersection func  "<< s.intersection(s2) << endl;
    cout << "merge func  "<< s.merge(s2) << endl;
    cout << "subtract func  "<< s.subtract(s2) << endl;
    cout << endl;
    merge = s + s2;
    intersec = s * s2;
    subtract = s / s2;
    cout <<"intersec *  "<< intersec <<endl;
    cout <<"merge +  "<< merge <<endl;
    cout <<"subtract /  "<< subtract <<endl;
    cout << endl;
    set<int>copyOfS;
    set<int>secondCopyOfS;
    copyOfS = s;
    secondCopyOfS = s;
    copyOfS*=s2;
    secondCopyOfS/=s2;
    s+=s2;
    cout << "intersection *=  "<< copyOfS << endl;
    cout <<"merge +=  "<< s <<endl;
    cout << "subtract /=  "<< secondCopyOfS << endl;
    cout << endl;
    cout<< arb<<endl;
    ar.add(99);
    ar.add(100);
    ar.add(10123);
    ar.add(122200);
    ar.add(-5);
    ar.add(10);
    ar.add(999);
    cout<<"with add elems   "<< ar << endl;
    ar.remove(999);
    cout<<"delete elem 999  " << ar << endl;

    Iterator<int>z(list);
    Iterator<int>z2(list);
    z2.next();
    cout <<"my cout <<   " << list <<endl;

    cout << "cout with iterator   ";
    for (Iterator<int> it=list.iterator_begin(); it!=list.iterator_end(); ++it)
        cout << *it<< ' ' ;
    cout << endl;
    cout <<"fiction elem with *   " <<*list.iterator_end()<<endl;
    cout <<"fiction elem with value()   "<<list.iterator_end().value()<<endl;
    cout <<"first elem with iterator_begin() and constructor   " <<*list.iterator_begin()<<" "<<*z<<endl;
    cout <<"second elem ( first.next() )"<<z2.value()<<endl;
    cout <<"check == (is .iterator_begin == constuctor 1st elem)   " << (list.iterator_begin() == z)<<endl;
    cout <<"check != (is .iterator_begin != iterator_end)   " << (list.iterator_begin() != list.iterator_end())<<endl;


    cout<<"check iterator_end().is_end()  " <<list.iterator_end().is_end()<<" "<<endl;
    cout<<"check elem befor fiction elem   "  <<*(--list.iterator_end())<<endl;



    cout<< "cout set 'ar'  " <<ar << endl;
    ar.clear();
    cout<< "cout set after 'ar' .clear()  " <<ar;
    return 0;
    //123
}
