#include "setofnumbers.h"
template <typename T>
set<T>::set()
{
    size=0;
    data = nullptr;
}


template <typename T>
set<T>::set(const set<T>& s)
{
    size = s.size;
    data = new T[size];
    for(int i = 0; i < size; i++)
        data[i] = s.data[i];
}

template <typename T>
set<T>::set(std::initializer_list<std::initializer_list<T>> lst)
{
    size = 0;
    for (auto smallList = lst.begin(); smallList != lst.end(); smallList++)
           size += smallList->size();
    data = new T[size];

    int i = 0;
    for (auto smallList = lst.begin(); smallList != lst.end(); smallList++)
        for (auto iter = smallList->begin(); iter != smallList->end(); iter++)
        {
            data[i] = *iter;
            i++;
        }

    sort(size, data);
    size = removeDuplicates(size, data);


}

template <typename T>
set<T>::set(initializer_list<T> lst)
{
    size = lst.size();
    data = new T[size];
    int i = 0;
    for (auto iter = lst.begin(); iter != lst.end(); iter++)
    {
            data[i] = *iter;
            i++;
    }
    sort(size, data);
    size = removeDuplicates(size, data);

}

template <typename T>
set<T>::~set()
{
    delete[] data;
    size=0;
}

template <typename T>
set<T>& set<T>::operator =(const set<T>& lst)
{
    size = lst.size;
    data = new T[size];
    for(int i = 0; i < size; i++)
        data[i] = lst.data[i];

    return *this;
}

template <class T>
int set<T>::get_length()
{
    return size;
}

template <typename T>
bool set<T>::contains(const T& elem)
{
    int i = 0;
    bool isntContain = true;
    while ((isntContain = (data[i] != elem)) && (i<size))
    {
       i++;
    }
    return !isntContain;
}

template <typename T>
void set<T>::add(const T& elem)
{
    if (!contains(elem))
    {
        size++;
        T* new_data = new T[size];
        int i = 0;
        for (i ; i < size - 1; i++)
            new_data[i] = data[i];
        new_data[i] = elem;
        data = new_data;

        sort(size, data);
    }
}

template <typename T>
void set<T>::remove(const T& elem)
{
    if  (contains(elem))
    {
        T* newdata = new T[size];
        int i = 0;
        while (data[i] != elem)
        {
            i++;
        }
        for (int j = i; j < size - 1; j++)
            data[j] = data[j+1];
        size--;
    }
}

template <typename T>
T* set<T>::to_array()
{
    T* array = new T[size];
    for(unsigned i = 0; i < size; i++ )
        array[i] = data[i];
    return array;
}

template <typename T>
set<T>& set<T>::merge(const set<T>& s) //union
{
    set<T> *mergeSet = new set<T>;
    *mergeSet = set<T>();
    for (int i = 0; i < s.size; i++)
        mergeSet->add(s.data[i]);

    for (int i = 0; i < size; i++)
        mergeSet->add(data[i]);

    return *mergeSet;

}

template <typename T>
set<T>& set<T>::intersection(const set<T>& s)
{

    int lengthCnt = 0;
    for(int i = 0; i < size; i++)
        for (int j = 0; j < s.size; j++)
        {
            if(data[i] == s.data[j])
            lengthCnt ++;
        }


    set<T> *interSet = new set<T>;
    *interSet = set<T>();
    interSet->data = new T[lengthCnt];
    interSet->size = lengthCnt;

    int newInd = 0;

    for(int i = 0; i < size; i++)
        for (int j = 0; j < s.size; j++)
        {
            if(data[i] == s.data[j])
            {
                interSet->data[newInd] = data[i];
                newInd++;
            }
        }
    return *interSet;
}

template <typename T>
set<T>& set<T>::subtract(const set<T>& s)//raznost
{

    set<T> *mergeSet = new set<T>;
    *mergeSet = merge(s);

    for (int i = 0; i< s.size; i++)
        mergeSet->remove(s.data[i]);

    return *mergeSet;
}

template <typename T>
T& set<T>::operator[](int index)
{
    if (index<size)
        return data[index];
    else
        cout<<"error, index>size";
}

template<typename _T>
set<_T> operator +(const set<_T>& s1, const set<_T>& s2)
{
    set<_T> *newSet = new set<_T>;
    *newSet = set<_T>();
    for (int i = 0; i < s1.size; i++)
        newSet->add(s1.data[i]);

    for (int i = 0; i < s2.size; i++)
        newSet->add(s2.data[i]);

    return *newSet;
}

template<typename _T>
set<_T> operator *(const set<_T>& s1, const set<_T>& s2)
{
    set<_T> *newSet = new set<_T>;
    *newSet = set<_T>();
    for (int i = 0; i < s1.size; i++)
        newSet->add(s1.data[i]);

    for (int i = 0; i < s2.size; i++)
        *newSet = newSet->intersection(s2);

    return *newSet;
}

template<typename _T>
set<_T> operator /(const set<_T>& s1, const set<_T>& s2)
{
    set<_T> *newSet = new set<_T>;
    *newSet = set<_T>();
    for (int i = 0; i < s1.size; i++)
        newSet->add(s1.data[i]);

    for (int i = 0; i < s2.size; i++)
        *newSet = newSet->subtract(s2);

    return *newSet;
}

template<typename T>
set<T>& set<T>::operator +=(const set<T>& s)
{
    *this = merge(s);
    return *this;
}

template<typename T>
set<T>& set<T>::operator *=(const set<T>& s)
{
    *this = intersection(s);
    return *this;
}

template<typename T>
set<T>& set<T>::operator /=(const set<T>& s)
{
    *this = subtract(s);
    return *this;
}

template <typename T>
ostream& operator <<(ostream& os,const set<T>& s)
{
    if(s.size == 0)
    {
        os << "Empty Set" << endl;
        return os;
    }
    for(int i = 0; i < s.size; i++)
        os << s.data[i] << " ";
    return os;
}

template <class T>
void set<T>::clear()
{
    this->size = 0;
}

