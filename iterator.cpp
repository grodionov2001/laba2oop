#include "iterator.h"
#include "setofnumbers.h"

    template <typename T>
    Iterator<T>::Iterator(const set<T>& s)
    {
        pointer = s.data;
        last = s.data + s.size;
        *last = NULL;
    }


    template <typename T>
    Iterator<T> set<T>::iterator_begin()
    {
        return Iterator<T> (*this);
    }

    template <typename T>
    Iterator<T> set<T>::iterator_end()
    {
        Iterator<T>result(*this);

        for (int i = 0; i < size; i++)
            ++result;
        return result;
    }

    template <typename T>
    Iterator<T>& Iterator<T>::next()
    {
        pointer++;
        return *this;
    }

    template <typename T>
    T Iterator<T>::value()
    {
        return *pointer;
    }

    template <typename T>
    bool Iterator<T>::operator != (Iterator<T> b)
    {
        return pointer != b.pointer;
    }

    template <typename T>
    bool Iterator<T>::operator == (Iterator<T> b)
    {
        return pointer == b.pointer;
    }

    template <typename T>
    Iterator<T>& Iterator<T>::operator++()
    {
        pointer++;
        return *this;
    }

    template <typename T>
    Iterator<T>& Iterator<T>::operator--()
    {
        pointer--;
        return *this;
    }

    template <typename T>
    T & Iterator<T>::operator*()
    {
        return *pointer;
    }

    template <typename T>
    bool Iterator<T>::is_end()
    {
        return  (pointer == last);
    }


