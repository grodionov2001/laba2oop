TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        iterator.cpp \
        main.cpp \
        setofnumbers.cpp

HEADERS += \
    iterator.h \
    setofnumbers.h
