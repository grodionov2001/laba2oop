#ifndef ITERATOR_H
#define ITERATOR_H
#include "setofnumbers.h"

template <typename T>
class Iterator
{
    private:
        T* pointer;
        T* last;
    public:
        Iterator<T>(const set<T>& s);
        Iterator<T>& next();
        T value();
        bool is_end();
        Iterator &operator++();
        Iterator &operator--();
        T &operator*();
        bool operator ==(Iterator b);
        bool operator !=(Iterator b);
};
#endif // ITERATOR_H
